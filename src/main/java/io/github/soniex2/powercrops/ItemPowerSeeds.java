package io.github.soniex2.powercrops;

import io.github.soniex2.powercrops.util.LithiumHelper;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.ICapabilityProvider;
import net.minecraftforge.common.util.Constants;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.IEnergyStorage;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.registries.IRegistryDelegate;

import javax.annotation.Nullable;
import java.util.ConcurrentModificationException;

/**
 * @author soniex2
 */
public class ItemPowerSeeds extends Item {
	public final int MAX_POWER = PowerCrops.instance.creationCost();
	public final int POWER_TRANSFER = MAX_POWER/100;

	public static final String ITEMID = "a_full_commitment_s_what_i_m_thinking_of";

	private final IRegistryDelegate<Item> craftedItem;

	public ItemPowerSeeds(Item craftedItem) {
		super();
		this.craftedItem = craftedItem.delegate;
		setUnlocalizedName(PowerCrops.MODID + "." + ITEMID);
		setRegistryName(new ResourceLocation(PowerCrops.MODID, ITEMID));
		setCreativeTab(CreativeTabs.MATERIALS);
	}

	@Nullable
	@Override
	public ICapabilityProvider initCapabilities(ItemStack stack,
	                                            @Nullable
		                                            NBTTagCompound nbt) {
		if (FMLCommonHandler.instance().getEffectiveSide() == Side.SERVER) {
			stack.getOrCreateSubCompound("powerseed").setInteger("max amount", MAX_POWER);
		}
		return super.initCapabilities(stack, nbt);
	}

	@Override
	public boolean onEntityItemUpdate(EntityItem entityItem) {
		ItemStack is = entityItem.getItem();
		if (is.getCount() <= 0) {
			return false;
		}
		NBTTagCompound compound = is.getOrCreateSubCompound("powerseed");
		int i = compound.getInteger("amount");
		if (!compound.hasKey("max amount", Constants.NBT.TAG_INT)) {
			compound.setInteger("max amount", MAX_POWER);
		}
		final int max = compound.getInteger("max amount");
		if (i >= max) {
			// TODO create new entity?
			entityItem.setItem(new ItemStack(craftedItem.get(), is.getCount()));
			entityItem.lifespan++;
			return false;
		}
		World w = entityItem.getEntityWorld();
		BlockPos pos = entityItem.getPosition().down();
		TileEntity te = w.getTileEntity(pos);
		if (te == null) {
			return false;
		}
		IEnergyStorage e = LithiumHelper.lithiumToForge(te, EnumFacing.UP, te.getCapability(CapabilityEnergy.ENERGY, EnumFacing.UP));
		if (e == null) {
			return false;
		}
		int l = Math.min(POWER_TRANSFER * is.getCount(), (max - i) * is.getCount());
		assert l >= 0;
		if (l == 0) {
			return false;
		}
		int available = e.extractEnergy(l, true);
		int request = (available / is.getCount()) * is.getCount(); // no, this isn't useless. integer div rounds to 0.
		int requested = e.extractEnergy(request, false);
		if (request != requested) {
			throw new ConcurrentModificationException("Erroneous energy duplication or deletion detected!");
		}
		int n = i + requested / is.getCount();
		assert n > 0 && n <= max;
		compound.setInteger("amount", n);
		entityItem.lifespan++;
		return false;
	}

	@Override
	public int getDamage(ItemStack stack) {
		if (FMLCommonHandler.instance().getEffectiveSide() == Side.CLIENT) {
			if (stack.getSubCompound("powerseed") == null) {
				return 0;
			}
		}
		NBTTagCompound compound = stack.getOrCreateSubCompound("powerseed");
		int i = compound.getInteger("amount");
		if (!compound.hasKey("max amount", Constants.NBT.TAG_INT)) {
			compound.setInteger("max amount", MAX_POWER);
		}
		final int max = compound.getInteger("max amount");
		if (i > max) {
			i = max;
			compound.setInteger("amount", i);
		}
		return i;
	}

	@Override
	public int getMaxDamage(ItemStack stack) {
		if (FMLCommonHandler.instance().getEffectiveSide() == Side.CLIENT) {
			if (stack.getSubCompound("powerseed") == null) {
				return 0;
			}
		}
		NBTTagCompound compound = stack.getOrCreateSubCompound("powerseed");
		if (!compound.hasKey("max amount", Constants.NBT.TAG_INT)) {
			compound.setInteger("max amount", MAX_POWER);
		}
		return compound.getInteger("max amount");
	}

	@Override
	public boolean showDurabilityBar(ItemStack stack) {
		return !(FMLCommonHandler.instance().getEffectiveSide() == Side.CLIENT && stack.getSubCompound("powerseed") == null);
	}

	@Override
	public double getDurabilityForDisplay(ItemStack stack) {
		if (FMLCommonHandler.instance().getEffectiveSide() == Side.CLIENT) {
			if (stack.getSubCompound("powerseed") == null) {
				return 0.0;
			}
		}
		NBTTagCompound compound = stack.getOrCreateSubCompound("powerseed");
		int i = compound.getInteger("amount");
		if (!compound.hasKey("max amount", Constants.NBT.TAG_INT)) {
			compound.setInteger("max amount", MAX_POWER);
		}
		final int max = compound.getInteger("max amount");
		if (i > max) {
			i = max;
			compound.setInteger("amount", i);
		}
		return 1.0 - (double) i / (double) max;
	}

	@Override
	public int getRGBDurabilityForDisplay(ItemStack stack) {
		return MathHelper.hsvToRGB(Math.max(0.0F, 1.0F - (float) getDurabilityForDisplay(stack)) / 3.0F, 1.0F, 1.0F);
	}
}
