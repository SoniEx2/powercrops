package io.github.soniex2.powercrops;

import net.minecraft.block.BlockCrops;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.EnumPlantType;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.IEnergyStorage;

import javax.annotation.Nullable;

/**
 * @author soniex2
 */
public class BlockPowerPlant extends BlockCrops implements ITileEntityProvider {

	public static final String BLOCKID = "we_re_no_strangers_to_love";
	private static final boolean SEEDS_DROP = PowerCrops.instance.seedsDrop();

	public BlockPowerPlant() {
		setRegistryName(new ResourceLocation(PowerCrops.MODID, BLOCKID));
		setUnlocalizedName(PowerCrops.MODID + "." + BLOCKID);
	}

	@Override
	protected Item getSeed() {
		return Items.AIR;
	}

	@Override
	protected Item getCrop() {
		return SEEDS_DROP ? PowerCrops.instance.getRealizedPowerSeeds() : Items.AIR;
	}

	@Override
	public boolean hasTileEntity(IBlockState state) {
		return true;
	}

	@Nullable
	@Override
	public TileEntity createTileEntity(World world, IBlockState state) {
		return new TileEntityPowerPlant();
	}

	@Nullable
	@Override
	public TileEntity createNewTileEntity(World worldIn, int meta) {
		return new TileEntityPowerPlant();
	}

	@Override
	public EnumPlantType getPlantType(IBlockAccess world, BlockPos pos) {
		return EnumPlantType.Crop;
	}

	@Override
	public void grow(World worldIn, BlockPos pos, IBlockState state) {
		int pre = getAge(state);
		super.grow(worldIn, pos, state);
		int post = getAge(worldIn.getBlockState(pos));
		int total = post - pre;
		if (total > 0) {
			TileEntity te = worldIn.getTileEntity(pos);
			if (te instanceof TileEntityPowerPlant) {
				((TileEntityPowerPlant) te).energyContainer.grow(total);
				te.markDirty();
			}
		}
	}
}
