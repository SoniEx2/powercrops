package io.github.soniex2.powercrops;

import net.minecraftforge.energy.EnergyStorage;

/**
 * @author soniex2
 */
public class PowerPlantEnergyStorage extends EnergyStorage {
	public PowerPlantEnergyStorage() {
		super(0);
	}

	public void grow() {
		energy += 10;
	}

	public void grow(int n) {
		energy += 10 * n;
	}

	@Override
	public int receiveEnergy(int maxReceive, boolean simulate) {
		return 0;
	}

	@Override
	public int extractEnergy(int maxExtract, boolean simulate) {
		int toextract = Math.min(maxExtract, energy);
		if (!simulate) {
			energy -= toextract;
		}
		return toextract;
	}

	@Override
	public int getEnergyStored() {
		return energy;
	}

	@Override
	public int getMaxEnergyStored() {
		return energy;
	}

	@Override
	public boolean canExtract() {
		return true;
	}

	@Override
	public boolean canReceive() {
		return false;
	}

	public void setEnergy(int energy) {
		this.energy = energy;
	}
}
