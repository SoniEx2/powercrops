package io.github.soniex2.powercrops;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.common.registry.GameRegistry.ObjectHolder;
import net.minecraftforge.fml.relauncher.ReflectionHelper;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.registries.ForgeRegistry;
import org.apache.logging.log4j.Logger;

import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.util.Map;

/**
 * @author soniex2
 */
@Mod(modid = PowerCrops.MODID, name = "You wouldn't get this from any other guy", version = "2.0.0")
public class PowerCrops {
	public static final String MODID = "powercrops";
	@Mod.Instance
	public static PowerCrops instance = null;

	@ObjectHolder(MODID + ":" + ItemPowerSeeds.ITEMID)
	private static final ItemPowerSeeds powerSeeds = null;
	@ObjectHolder(MODID + ":" + ItemRealizedPowerSeeds.ITEMID)
	private static final ItemRealizedPowerSeeds realizedPowerSeeds = null;
	@ObjectHolder(MODID + ":" + BlockPowerPlant.BLOCKID)
	private static final BlockPowerPlant powerPlant = null;

	private Logger log;

	private boolean seedsDrop = true;
	private int genRate = 10;
	private int creationCost = 10000;

	public Logger getLog() {
		return log;
	}

	@Mod.EventHandler
	public void yUNoPerMethodDependencies(FMLPreInitializationEvent event) {
		File dir = new File(event.getModConfigurationDirectory(), "i_just_wanna_tell_you_how_i_m_feeling");
		dir.mkdir();
		File file = new File(dir, "gotta_make_you_understand.cfg");
		if (!dir.isDirectory()) {
			do { // who said java doesn't have goto?
				if (Desktop.isDesktopSupported()) {
					Desktop desktop = Desktop.getDesktop();
					if (desktop.isSupported(Desktop.Action.BROWSE)) {
						URI uri = URI.create("https://www.youtube.com/watch?v=dQw4w9WgXcQ");
						try {
							desktop.browse(uri);
							break; // goto exit;
						} catch (IOException ignored) {
						}
					}
				}
				System.out.println("https://www.youtube.com/watch?v=dQw4w9WgXcQ");
			} while (false);
			// exit:
			throw new RuntimeException("Never gonna give you up, "
				+ "Never gonna let you down, "
				+ "Never gonna run around and desert you", new FileNotFoundException(file.getAbsolutePath()));
		}

		Configuration config = new Configuration(file);
		config.load();
		genRate = config.getInt("GenRate", Configuration.CATEGORY_GENERAL, genRate, 1, Integer.MAX_VALUE, "Energy generated per growth tick.");
		creationCost = config.getInt("CreationCost", Configuration.CATEGORY_GENERAL, creationCost, 0, Integer.MAX_VALUE, "Energy cost of a power seed. (0 = no energy cost)");
		seedsDrop = config.getBoolean("SeedsDrop", Configuration.CATEGORY_GENERAL, seedsDrop, "Whether seeds drop from fully grown PowerCrops.");
		config.save();

		// register item/block registration
		MinecraftForge.EVENT_BUS.register(this);

		MinecraftForge.EVENT_BUS.register(new Rick());

		log = event.getModLog();
	}


	@SubscribeEvent
	public void registerBlocks(RegistryEvent.Register<Block> event) {
		event.getRegistry().register(new BlockPowerPlant());
		GameRegistry.registerTileEntity(TileEntityPowerPlant.class, MODID + ":hcf");
	}

	@SubscribeEvent
	public void registerItems(RegistryEvent.Register<Item> event) {
		Item realizedSeeds;
		event.getRegistry().register(realizedSeeds = new ItemRealizedPowerSeeds(powerPlant));
		event.getRegistry().register(new ItemPowerSeeds(realizedSeeds));
	}

	@SubscribeEvent
	@SideOnly(Side.CLIENT)
	public void registerModels(ModelRegistryEvent event) {
		ModelLoader.setCustomModelResourceLocation(powerSeeds, 0, new ModelResourceLocation(new ResourceLocation(MODID, ItemPowerSeeds.ITEMID), ""));
		ModelLoader.setCustomModelResourceLocation(realizedPowerSeeds, 0, new ModelResourceLocation(new ResourceLocation(MODID, ItemRealizedPowerSeeds.ITEMID), ""));
	}

	/*@SubscribeEvent
	public void registerRecipes(RegistryEvent.Register<IRecipe> event) {
		// TODO JSON
		//GameRegistry.addShapedRecipe(new ResourceLocation(MODID, "powerseeds"), new ResourceLocation(MODID, MODID), powerSeeds.MAX_POWER == 0 ? new ItemStack(realizedPowerSeeds) : new ItemStack(powerSeeds), "xxx", "xyx", "xxx", 'x', Items.REDSTONE, 'y', Items.WHEAT_SEEDS);
	}*/

	public BlockPowerPlant getPowerPlant() {
		return powerPlant;
	}

	public ItemRealizedPowerSeeds getRealizedPowerSeeds() {
		return realizedPowerSeeds;
	}


	public int genRate() {
		return genRate;
	}

	public int creationCost() {
		return creationCost;
	}

	public boolean seedsDrop() {
		return seedsDrop;
	}
}
