package io.github.soniex2.powercrops;

import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemSeeds;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import javax.annotation.Nonnull;

/**
 * @author soniex2
 */
public class ItemRealizedPowerSeeds extends ItemSeeds {
	public static final String ITEMID = "you_know_the_rules_and_so_do_i";

	@GameRegistry.ObjectHolder(PowerCrops.MODID + ":" + BlockPowerPlant.BLOCKID)
	public static Block powerPlant;

	public ItemRealizedPowerSeeds(@Nonnull Block crops) {
		super(crops, Blocks.FARMLAND);
		setUnlocalizedName(PowerCrops.MODID + "." + ITEMID);
		setRegistryName(new ResourceLocation(PowerCrops.MODID, ITEMID));
	}

	@SideOnly(Side.CLIENT)
	@Override
	public boolean hasEffect(ItemStack stack) {
		return true;
	}
}
