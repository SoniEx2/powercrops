package io.github.soniex2.powercrops;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.IEnergyStorage;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;

/**
 * @author soniex2
 */
public class Rick {
	@SubscribeEvent
	public void roll(BlockEvent.CropGrowEvent.Post event) {
		if (event.getState().getBlock() == PowerCrops.instance.getPowerPlant()) {
			TileEntity te = event.getWorld().getTileEntity(event.getPos());
			if (te instanceof TileEntityPowerPlant) {
				((TileEntityPowerPlant) te).energyContainer.grow();
				te.markDirty();
			}
		}
	}
}
