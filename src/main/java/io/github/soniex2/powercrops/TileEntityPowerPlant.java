package io.github.soniex2.powercrops;

import io.github.soniex2.powercrops.util.LithiumHelper;
import net.minecraft.block.state.IBlockState;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.IEnergyStorage;

import javax.annotation.Nullable;
import java.util.ConcurrentModificationException;

/**
 * @author soniex2
 */
public class TileEntityPowerPlant extends TileEntity implements ITickable {
	public LithiumHelper.PowerPlantEnergyContainer energyContainer = LithiumHelper.createPowerPlantEnergyContainer();

	@Override
	public boolean hasCapability(Capability<?> capability,
	                             @Nullable
		                             EnumFacing facing) {
		return energyContainer.compatibleCapability(capability) || super.hasCapability(capability, facing);
	}

	@Nullable
	@Override
	public <T> T getCapability(Capability<T> capability,
	                           @Nullable
		                           EnumFacing facing) {
		return energyContainer.intoCapability(capability, super.getCapability(capability, facing));
	}

	@Override
	public void update() {
		if (energyContainer.getEnergy() == 0) {
			return;
		}
		for (EnumFacing f : EnumFacing.VALUES) {
			TileEntity te = world.getTileEntity(pos.offset(f));
			if (te != null) {
				IEnergyStorage energyStorage = te.getCapability(CapabilityEnergy.ENERGY, f.getOpposite());
				if (energyStorage != null) {
					try {
						energyContainer.moveEnergy(energyStorage);
					} finally {
						// no idea if this does anything.
						te.markDirty();
						this.markDirty();
					}
				}
			}
		}
	}

	@Override
	public void readFromNBT(NBTTagCompound compound) {
		super.readFromNBT(compound);
		energyContainer.setEnergy(compound.getInteger("powerplant_energy"));
	}

	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound compound) {
		compound = super.writeToNBT(compound);
		compound.setInteger("powerplant_energy", energyContainer.getEnergy());
		return compound;
	}

	@Override
	public boolean shouldRefresh(World world, BlockPos pos, IBlockState oldState, IBlockState newSate) {
		return oldState.getBlock() != newSate.getBlock();
	}
}
