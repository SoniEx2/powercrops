package io.github.soniex2.powercrops.util;

/*import io.github.soniex2.lithium.api.action.IExtractAction;
import io.github.soniex2.lithium.api.action.IInsertAction;
import io.github.soniex2.lithium.api.energy.IEnergyHolder;
import io.github.soniex2.lithium.api.energy.IEnergyProvider;
import io.github.soniex2.lithium.api.energy.IEnergyReceiver;
import io.github.soniex2.lithium.api.energy.impl.SimpleEnergyHolder;*/
import io.github.soniex2.powercrops.PowerCrops;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;
//import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.EnergyStorage;
import net.minecraftforge.energy.IEnergyStorage;

import java.util.ConcurrentModificationException;

/**
 * @author soniex2
 */
public class LithiumHelper {
	private static final int GEN_RATE = PowerCrops.instance.genRate();

	//@CapabilityInject(IEnergyProvider.class)
	private static Capability<?> ENERGY_PROVIDER = null;

	//@CapabilityInject(IEnergyReceiver.class)
	private static Capability<?> ENERGY_RECEIVER = null;

	//@CapabilityInject(IEnergyHolder.class)
	private static Capability<?> ENERGY_HOLDER = null;

	public static IEnergyStorage lithiumToForge(TileEntity te, EnumFacing side, IEnergyStorage fb) {
		if (ENERGY_PROVIDER == null || ENERGY_RECEIVER == null || ENERGY_HOLDER == null) {
			return fb;
		}
		//final IEnergyProvider energyProvider = (IEnergyProvider) te.getCapability(ENERGY_PROVIDER, side);
		//final IEnergyReceiver energyReceiver = (IEnergyReceiver) te.getCapability(ENERGY_RECEIVER, side);
		//final IEnergyHolder energyHolder = (IEnergyHolder) te.getCapability(ENERGY_HOLDER, side);
		//return new LithiumForgeWrapper(energyProvider, energyReceiver, energyHolder);
		return null;
	}

	public static PowerPlantEnergyContainer createPowerPlantEnergyContainer() {
		if (ENERGY_PROVIDER == null || ENERGY_RECEIVER == null || ENERGY_HOLDER == null) {
			PowerCrops.instance.getLog().info("Creating new Forge energy container");
			return new ForgeEnergyContainer();
		} else {
			PowerCrops.instance.getLog().info("Creating new Lithium energy container");
			//return new LithiumEnergyContainer();
			return null;
		}
	}

	public interface PowerPlantEnergyContainer {
		void grow();

		void grow(int n);

		<T> boolean compatibleCapability(Capability<T> cap);

		<T> T intoCapability(Capability<T> cap, T fb);

		void setEnergy(int energy);

		int getEnergy();

		void moveEnergy(IEnergyStorage target);
	}

	private static class ForgeEnergyContainer extends EnergyStorage implements PowerPlantEnergyContainer {
		private ForgeEnergyContainer() {
			super(0);
		}

		@Override
		public void grow() {
			energy += GEN_RATE;
		}

		@Override
		public void grow(int n) {
			energy += GEN_RATE * n;
		}

		@Override
		public <T> boolean compatibleCapability(Capability<T> cap) {
			return cap == CapabilityEnergy.ENERGY;
		}

		@Override
		public <T> T intoCapability(Capability<T> cap, T fb) {
			if (cap == CapabilityEnergy.ENERGY) {
				return (T) this;
			}
			return fb;
		}

		@Override
		public int receiveEnergy(int maxReceive, boolean simulate) {
			return 0;
		}

		@Override
		public int extractEnergy(int maxExtract, boolean simulate) {
			int toextract = Math.min(maxExtract, energy);
			if (!simulate) {
				energy -= toextract;
			}
			return toextract;
		}

		@Override
		public int getEnergyStored() {
			return energy;
		}

		@Override
		public int getMaxEnergyStored() {
			return energy;
		}

		@Override
		public boolean canExtract() {
			return true;
		}

		@Override
		public boolean canReceive() {
			return false;
		}

		@Override
		public void setEnergy(int energy) {
			this.energy = energy;
		}

		@Override
		public int getEnergy() {
			return getEnergyStored();
		}

		@Override
		public void moveEnergy(IEnergyStorage target) {
			int toExtract = this.extractEnergy(Integer.MAX_VALUE, true);
			int inserted = target.receiveEnergy(toExtract, false);
			int extracted = this.extractEnergy(inserted, false);
			if (inserted != extracted) {
				throw new ConcurrentModificationException("Erroneous energy duplication or deletion detected!");
			}
		}
	}

	/*private static class LithiumEnergyContainer extends SimpleEnergyHolder implements PowerPlantEnergyContainer {
		private final IEnergyStorage forgeWrapper;

		private LithiumEnergyContainer() {
			super(Integer.MAX_VALUE, Integer.MAX_VALUE, 0);
			forgeWrapper = new LithiumForgeWrapper(this, this, this);
		}

		@Override
		public void grow() {
			this.energy.receive(10).commit();
		}

		@Override
		public void grow(int n) {
			this.energy.receive(10 * n).commit();
		}

		@Override
		public <T> boolean compatibleCapability(Capability<T> cap) {
			return cap == CapabilityEnergy.ENERGY ||
				cap == ENERGY_HOLDER ||
				cap == ENERGY_PROVIDER ||
				cap == ENERGY_RECEIVER;
		}

		@Override
		public <T> T intoCapability(Capability<T> cap, T fb) {
			if (cap == ENERGY_PROVIDER || cap == ENERGY_RECEIVER || cap == ENERGY_HOLDER) {
				return (T) this;
			} else if (cap == CapabilityEnergy.ENERGY) {
				return (T) forgeWrapper;
			}
			return fb;
		}

		@Override
		public void setEnergy(int energy) {
			this.energy.extract(Integer.MAX_VALUE).commit();
			this.energy.receive(energy).commit();
		}

		@Override
		public int getEnergy() {
			return energy.getEnergy();
		}

		@Override
		public void moveEnergy(IEnergyStorage target) {
			IExtractAction toExtract = this.extract(Integer.MAX_VALUE);
			toExtract.revert();
			int inserted = target.receiveEnergy(toExtract.getEnergy(), false);
			IExtractAction extracted = this.extract(inserted);
			extracted.commit();
			if (inserted != extracted.getEnergy()) {
				throw new ConcurrentModificationException("Erroneous energy duplication or deletion detected!");
			}
		}
	}

	private static class LithiumForgeWrapper implements IEnergyStorage {
		private final IEnergyProvider energyProvider;
		private final IEnergyReceiver energyReceiver;
		private final IEnergyHolder energyHolder;

		private LithiumForgeWrapper(IEnergyProvider energyProvider, IEnergyReceiver energyReceiver, IEnergyHolder energyHolder) {
			this.energyProvider = energyProvider;
			this.energyReceiver = energyReceiver;
			this.energyHolder = energyHolder;
		}

		@Override
		public int receiveEnergy(int maxReceive, boolean simulate) {
			if (energyReceiver == null) {
				return 0;
			}
			IInsertAction action = energyReceiver.receive(maxReceive);
			if (simulate) {
				action.revert();
			} else {
				action.commit();
			}
			return action.getEnergy();
		}

		@Override
		public int extractEnergy(int maxExtract, boolean simulate) {
			if (energyProvider == null) {
				return 0;
			}
			IExtractAction action = energyProvider.extract(maxExtract);
			if (simulate) {
				action.revert();
			} else {
				action.commit();
			}
			return action.getEnergy();
		}

		@Override
		public int getEnergyStored() {
			if (energyHolder == null) {
				return 0;
			}
			return energyHolder.getCurrentEnergy(0);
		}

		@Override
		public int getMaxEnergyStored() {
			if (energyHolder == null) {
				return 0;
			}
			return energyHolder.getEnergyLimit(0);
		}

		@Override
		public boolean canExtract() {
			return energyProvider != null;
		}

		@Override
		public boolean canReceive() {
			return energyReceiver != null;
		}
	}*/
}
