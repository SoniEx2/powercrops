package powercrops;

import com.google.gson.JsonObject;
import io.github.soniex2.powercrops.ItemPowerSeeds;
import io.github.soniex2.powercrops.PowerCrops;
import net.minecraftforge.common.crafting.IConditionFactory;
import net.minecraftforge.common.crafting.JsonContext;
import net.minecraftforge.fml.common.registry.GameRegistry;

import java.util.function.BooleanSupplier;

/**
 * Yes I am too lazy to type out the whole package name for my main mod. This is why registering this bloody class should use an event and not a fucking _factories.json!
 * @author soniex2
 */
public class ZeroMaxEnergy implements IConditionFactory {
	@GameRegistry.ObjectHolder(PowerCrops.MODID + ":" + ItemPowerSeeds.ITEMID)
	public static final ItemPowerSeeds powerSeeds = null;

	@Override
	public BooleanSupplier parse(JsonContext context, JsonObject json) {
		return () -> powerSeeds.MAX_POWER == 0;
	}
}
